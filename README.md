# PAN Selection algorithm

Develop a collision free PAN (Primary Account Number) selection algorithm (in any preferred language) that allows to generate a (pseudo) random PAN number. PAN number contains 16 digits where first 6 digits are BIN (Bank Identification Number), digits 7 and 8 are Sub BIN range that should be configurable, digits 9-10 might be a product range and last digit is a check digit compatible with a result of using Luhn algorithm. 

## Examples:

For parameters of: 

```
BIN: 111111
Sub BIN: 31-51 (digit 7-8)
Product: 8-14 (digits 9-10)
```


```
Effective Range:

 BIN    7th  8th      9th - 10th             11th-15th(fr)    C
111111  3-5  0-9      08 - 14                00000-99999      C
                      (08,09,10,11,12,13,14)
Valid PAN numbers:

BIN     7th  8th      9th - 10th             11th-15th(fr)    C
111111  3     9       0      8               81945            C = 111111390881945C
111111  5     1       1      3               45663            C = 111111511345663C                      

Invalid PAN numbers

BIN     7th  8th      9th - 10th             11th-15th(fr)    C
211111  1     9       0      8               81945            C = 211111190881945C
111111  5     1       3      3               45663            C = 111111513345663C


```

## Requirements

- PAN numbers can't duplicate. You can't generate a PAN number that has already been selected.
- Once you generate the PAN number, you can't check in the database if the PAN number already has been generated. 
- Testing
- How to run

## Tip

You can use pre-populated ranges of available PAN numbers to select from.


